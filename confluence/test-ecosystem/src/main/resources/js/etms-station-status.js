function arrayToObject( array )
{
   var object = new Object();
   var i = 0;
   if( array != null){
      for( i = 0; i < array.length; i+=2 )
      {
         object[array[i]] = array[i+1];
      }
   }
   return object;
}

var EtmsStationStatusFields = (function($) {
    // module variables
    var methods = new Object();

    var EtmsStationStatusConfig = function() {};

    EtmsStationStatusConfig.prototype.fields = {
        "string": {
            "hostnames": function(param, options) {
                var paramDiv = $(Keysight.Test.Ecosystem.ETMS.Station.Status.Macro.Templates.textarea({rows:3}));
                var input = $("textarea", paramDiv);

                return new AJS.MacroBrowser.Field(paramDiv, input, options);
            },
            "show-fields": function(param, options) {
                var paramDiv = $(Keysight.Test.Ecosystem.ETMS.Station.Status.Macro.Templates.textarea({rows:3}));
                var input = $("textarea", paramDiv);

                return new AJS.MacroBrowser.Field(paramDiv, input, options);
            }
        }
    }

    AJS.MacroBrowser.Macros["etms-station-status"] = new EtmsStationStatusConfig();
})(AJS.$ || jQuery);

var etmsStationStatusFactory = (function($) {

    var methods = new Object();
    var stations = new Object();
    var url = AJS.contextPath() + "/rest/database/1.0/admin-config/profiles";

    methods['build'] = function() {
        $(".etms-station-status-body-container").each( function( index ){
            var hostname = $(this).attr( "hostname" );
            stations[ hostname ] = (function($, target, hostname){
                var methods = new Object();
                var refreshInterval = parseFloat($(target).attr('refresh-interval'));
                var bypassProxyText = $(target).attr('bypass-proxy');
                var bypassProxy = false;
                if( bypassProxyText != null && bypassProxyText == "true"){
                   bypassProxy = true;
                }

                methods['clear'] = function(){
                   $(target).find(".etms-station-status-results-container").remove();
                }

                methods['setReloadTimer'] = function(){
                   if( refreshInterval != null && refreshInterval > 0 ){
                      setTimeout( methods['reload'], refreshInterval * 60 * 1000);
                   }
                }

                methods['reload'] = function(){
                   methods.clear();
                   methods.load();
                }

                methods['load'] = function(){
                    $( ".ess-status", target ).spin();

                    var url = AJS.contextPath() + "/rest/test-ecosystem/1.0/proxy/Get/" + hostname;
                    var dataType = "json";
                    var jsonpCallback = false;

                    if( bypassProxy ){
                       url = "http://" + hostname + ":3867/EtmsWebServices/rest/Get";
                       dataType = "jsonp";
                       jsonpCallback = "callback";
                    }

                    AJS.$.ajax({
                        async: true,
                        url: url,
                        dataType: dataType,
                        data: {keyValueCsvString: "context,list"},
                        timeout: 10000, // 10 seconds,
                        jsonp: jsonpCallback,
                        error: function(xhr, textStatus, errorThrown) {
                            $(target).append(Keysight.Test.Ecosystem.ETMS.Station.Status.Macro.Templates.noinstances());
                            $( ".ess-status", target ).spinStop();
                        },
                        success: function(data) {
                            var listInfo = arrayToObject( data );
                            var instances;
                            var instanceState = new Object();
                            if( listInfo['instanceIds'] != null ){
                               instances = listInfo['instanceIds'].split(",");
                            }

                            if( instances == null ){
                                $(target).append(Keysight.Test.Ecosystem.ETMS.Station.Status.Macro.Templates.noinstances());
                            } else {
                                $(target).append(Keysight.Test.Ecosystem.ETMS.Station.Status.Macro.Templates.table({instances:instances}));
                                for( var j = 0; j < instances.length; j++ ){
                                    var currentInstance = instances[j];
                                    AJS.$.ajax({
                                        async: true,
                                        url: url,
                                        dataType: dataType,
                                        data: {keyValueCsvString: "context,state,processId,"+currentInstance},
                                        timeout: 10000, // 10 seconds,
                                        jsonp: jsonpCallback,
                                        error: function(xhr, textStatus, errorThrown) {
                                            console.error("Couldn't get state for " + currentInstance, errorThrown);
                                        },
                                        success: function(data) {
                                           var ok = true;
                                           var parsedData = arrayToObject(data);
                                           instanceState[parsedData.processId] = parsedData;

                                           for( var k = 0; k < instances.length; k++ ){
                                              if( instanceState[instances[k]] == null )
                                              {
                                                 ok = false
                                              }
                                           }

                                           if( ok ){
                                              var keys;
                                              if( $(target).attr('fields') != "*" ){
                                                 keys = $(target).attr('fields').split(",");
                                              } else {
                                                 keys = getSortedKeys( instanceState );
                                              }

                                              for( var j = 0; j < keys.length; j++){
                                                 var values = new Array();
                                                 for( var l = 0; l < instances.length; l++){
                                                    values.push( instanceState[instances[l]][keys[j]]);
                                                 }
                                                 $(target).find("tbody").append(Keysight.Test.Ecosystem.ETMS.Station.Status.Macro.Templates.tableRow({key:keys[j],values:values}));
                                              }
                                           }
                                        }
                                    });
                                }
                            }

                            methods['setReloadTimer']();
                            $( ".ess-status", target ).spinStop();
                        }
                    });
                }

                function getSortedKeys( instances )
                {
                    var uniqueKeys = new Object();
                    var instanceIds = Object.keys( instances )
                    for( var i = 0; i < instanceIds.length; i++){
                       var keys = Object.keys( instances[instanceIds[i]] )
                       for( var j = 0; j < keys.length; j++){
                           uniqueKeys[keys[j]] = 1
                       }
                    }
                    return Object.keys( uniqueKeys ).sort();
                }

                return methods;
            })($, $(this), hostname);
            stations[ hostname ].load();
        });
    }

    methods['helloWorld'] = function(profileId) {
        alert("hello world");
    }

    methods['getStationStatuses'] = function(profileId) {
    }

    return methods;
})(AJS.$ || jQuery);

AJS.toInit(function() {

    //AJS.$("#add-connection-profile").click(function(e) {
        //e.preventDefault();
        //profilesConfigHelper.addConnectionProfile();
    //});

    etmsStationStatusFactory.build();
});

