package com.keysight.database.helpers;

import com.atlassian.confluence.macro.MacroExecutionException;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Objects;

public class DatabaseQueryHelper {
    public static Connection createConnection(String profileId, PluginConfigManager pluginConfigManager) throws MacroExecutionException {
        // Get all stored profiles

        // needed for Confluence Data Center as the config could have been
        // updated by an instance of Confluence other than this one.
        pluginConfigManager.loadFromStorage();

        ConnectionProfile profile = pluginConfigManager.getConnectionProfileFromId( profileId );

        if (profile == null) {
            throw new MacroExecutionException("No profile found with that ID for which you are authorized: " + profileId);
        }

        return createConnection(profile, pluginConfigManager);
    }

    public static Connection createConnection (ConnectionProfile profile, PluginConfigManager pluginConfigManager) throws MacroExecutionException {
        Connection connect;
        String username = profile.getDatabaseUsername();
        String password = profile.getDatabasePassword();

        if (StringUtils.isEmpty(password) || Objects.equals(password, "*****")) {
            // Fetch the password from the authmap, where the profile name is the key.
            password = pluginConfigManager.getAuthMap().get(profile.getProfileId());
        }

        String subProtocol = StringUtils.lowerCase(profile.getDatabaseType());
        String connectionString = null;

        if (subProtocol.equals("microsoft sql server")) {
            subProtocol = "sqlserver";
        } else if(subProtocol.equals("jtds sql server")) {
            subProtocol = "jtds:sqlserver";
        } else if (subProtocol.equals("mongodb")) {
            subProtocol = "mongo";
        }

        if( subProtocol.equals("oracle") ) {
            String oraclePort = "1521";
            if (!StringUtils.isEmpty(profile.getDatabasePort())) {
                oraclePort = profile.getDatabasePort();
            }
            connectionString = "jdbc:" + subProtocol + ":thin:"
                    + "@" + profile.getDatabaseServer() + ":"
                    + oraclePort + ":"
                    + profile.getDatabaseName();
        } else {
            connectionString = "jdbc:" + subProtocol + "://" + profile.getDatabaseServer();
            if (!StringUtils.isEmpty(profile.getDatabasePort())) {
                connectionString += ":" + profile.getDatabasePort();
            }

            if (profile.getDatabaseType().matches("Microsoft SQL Server")) {
                connectionString += ";databaseName=" + profile.getDatabaseName();
            } else if (profile.getDatabaseType().matches("jTDS SQL Server")) {
                connectionString += ";databaseName=" + profile.getDatabaseName();
            } else {
                connectionString += "/" + profile.getDatabaseName();
            }
        }

        if (!StringUtils.isEmpty(profile.getConnectionStringSuffix())) {
            if (profile.getDatabaseType().matches("Microsoft SQL Server")) {
                connectionString += ";" + profile.getConnectionStringSuffix();
            } else if (profile.getDatabaseType().matches("jTDS SQL Server")) {
                connectionString += ";" + profile.getConnectionStringSuffix();
            } else {
                connectionString += "?" + profile.getConnectionStringSuffix();
            }
        }

        //System.out.println( "Connection String: " + connectionString );

        try {
            connect = DriverManager.getConnection(connectionString, username, password);
        } catch (SQLException exception) {
            if (exception.getMessage().matches(".*No suitable driver found.*")) {
                pluginConfigManager.loadJdbcDriver(profile.getDatabaseType());
                try {
                    connect = DriverManager.getConnection(connectionString, username, password);
                } catch (Exception exception2) {
                    throw new MacroExecutionException(exception2);
                }
            } else {
                throw new MacroExecutionException(exception);
            }
        } catch (Exception exception) {
            throw new MacroExecutionException(exception);
        }

        return connect;
    }

}
