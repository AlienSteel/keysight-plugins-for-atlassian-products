package com.keysight.api;

public interface MyPluginComponent
{
    String getName();
}